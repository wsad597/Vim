":so %	 to reload vimrc

" Vim-Plug
call plug#begin('~/.vim/plugged')
	Plug 'sjl/badwolf'	"color
	Plug 'Yggdroot/indentLine'	"show indent guide
	Plug 'ctrlpvim/ctrlp.vim'	"fuzzy file searcher
	Plug 'tomtom/tcomment_vim'	"comment with text objects, gc{c | motion} to comment
	" Plug 'jeffkreeftmeijer/vim-numbertoggle'	"auto toggle relative numbering
	Plug 'tpope/vim-unimpaired'	"provide mapping pairs for navigating buffers, arglist, etc
	" Plug 'tpope/vim-obsession' "store buffer layout between sessions
	Plug 'airblade/vim-gitgutter'	"show git status of lines in the left gutter
	Plug 'tpope/vim-fugitive' "use git from inside of vim
	" Plug 'tpope/vim-surround'	"act on surrounding objects like quotes etc
	" Plug 'davidhalter/jedi-vim' "python autocompletion, may need pip install Jedi
	" Plug 'plytophogy/vim-virtualenv' "work with python virtual env, seems to just work in vim
	Plug 'vim-airline/vim-airline' "nice status bar at bottom
	Plug 'vim-airline/vim-airline-themes' "used to change theme to badwolf
	Plug 'w0rp/ale'	"linter for lots of file types
	Plug 'sirtaj/vim-openscad' "syntax highlight file for openscad

	" Plug 'jacoborus/tender'	"color
	" Plug 'morhetz/gruvbox'	"color
	" Plug 'altercation/vim-colors-solarized'	"color
	" Plug 'dracula/vim'	"color
	" Plug 'blueshirts/darcula' "color, pycharm default dark
	" Plug 'dikiaap/minimalist'	"color
	" Plug 'nanotech/jellybeans'	"color
	" Plug 'nightsense/carbonized'	"color
	" Plug 'danilo-augusto/vim-afterglow'	"color
call plug#end()

" used with nerdcommenter
filetype plugin on
" let g:NERDCommentEmptyLines = 1

" used with gitgutter
let g:gitgutter_enabled = 0	"start disabled

" used with ale
let g:ale_enabled = 1	"start enabled, disable if slow

" used with indentLine
let g:indentLine_enabled = 1	" shouldnt need this, but sometimes doesnt show

" used with airline
" let g:airline_powerline_fonts = 1 "use this if we want to use powerline
let g:airline_theme='badwolf'
let g:airline#extensions#whitespace#enabled = 0 "this is to stop whitespace warnings

" let g:airline_section_x = airline#section#create(['(%{virtualenv#statusline()}) ','filetype'])

" used with virtualenv
" let g:virtualenv_directory = "G:/Users/DJM/Anaconda2/envs/"

" used with ctrlp
" r - nearest directory that contains .git
" a - direcoty of current file but only if working directory is not related
let g:ctrlp_working_path_mode = 'ra'

" used with defualt netrw file explorer
let g:netrw_liststyle = 3	"use tree view
" let g:netrw_banner = 0	"hide banner by defualt

" this was already here by default in my vimrc file, dont know
" source $VIMRUNTIME/vimrc_example.vim

set hidden	" let user leave file even if not saved
syntax enable	"I think this is on by default
" let python_highlight_all = 1	"this bascially turns on python lint
set showmatch	"I think this is on by default
set hlsearch
set ignorecase
set smartcase	"search and ignore case unless capital is used
colorscheme badwolf	"torte
" let g:gruvbox_contrast_dark= 'hard'
" let g:gruvbox_contrast_light= 'soft'
set background=dark
set number
" set number relativenumber	"show relative numbers with current line absolute

set backspace=indent,eol,start  " more powerful backspacing
set autoindent	" indent line if previous already indented
set tabstop=4
set shiftwidth=4
set softtabstop=4
"set smartindent	"basically only works for c programs, but auto tabs after functions and so on
set clipboard=unnamed	"let yank and past go to system clipboard
set textwidth=0 wrapmargin=0
set visualbell	"isntead of ding sound, flash screen white when error
set autoread	"load external file changes automatically
" set wildmenu
" set wildmode=longest,list	"set at suggestion of practical vim book
" set wildmode=longest:full,full
set colorcolumn=80	"make vertival line to let me know about edge of screen
set wrap formatoptions-=t linebreak	"visually wraps but not insert newline
set spelllang=en_gb	"set spell language but leave it off by default
" set spellsuggest=best,10  "set spell suggestion limit to 10 when press z=
set foldlevelstart=99

" enable unicode, mainly have this so airline works
if has("multi_byte")
	if &termencoding == ""
		let &termencoding = &encoding
	endif
	set encoding=utf-8
	setglobal fileencoding=utf-8
	" setglobal bomb
	set fileencodings=ucs-bom,utf-8,latin1
endif


if has('win32')
	" save temp files to the temp folder in windows
	set directory=$HOME/Temp//
	set backupdir=$HOME/Temp//
	set undodir=$HOME/Temp//
else
	set directory=~/.vim/.tmp//
	set backupdir=~/.vim/.tmp//
	set undodir=~/.vim/.tmp//
endif

" so that C-a and C-x will add and subtract decimals, not octals
set nrformats=

" print line numbers when printing
set printoptions=number:y

" already set
set incsearch	"highlight matches while typing

" only set if running in gui
if has('gui_running')
	" after search, un-highlights results when you press return
	nnoremap <silent> <CR> :nohlsearch<CR><CR>
	" sets font when using gui, maybe pick a setting to use when not on windows
	" set guifont=Consolas:h11:cANSI:qDRAFT
	if has('win32')
		set guifont=Consolas:h11:cANSI:qDRAFT
	else
		set guifont=Hack\ 11
	endif
endif

" START KEY MAPPINGS
let mapleader = ","

if has('win32')
	" open file explorer at current file directory
	noremap <F6> :!start explorer /select,%:p<CR>
	" open command promt at current file directory
	noremap <S-F6> :!start cmd /select,%:p<CR>
else
	" open file explorer at current file directory
	noremap <F6> :!xdg-open %:p:h<CR>
	" open command promt at current file directory
	" this doesn't work like I would like
	noremap <S-F6> :!konsole %:p:h<CR>
endif

" Ale Toggle -- toggle ALE linting on/off
nnoremap <Leader>at :ALEToggle<CR>

" Git Gutter Toggle -- toggle git gutter on/off
nnoremap <Leader>ggt :GitGutterToggle<CR>

" Spell Toggle -- toggle spell check in current file
nnoremap <Leader>st :setlocal spell!<CR>

" replaces %% with path of current file when in command line prompt
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" END KEY MAPPINGS
